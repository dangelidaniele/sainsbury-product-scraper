Sainsbury's Product Page Consumer

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/dangelidaniele/sainsbury-product-scraper/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/dangelidaniele/sainsbury-product-scraper/?branch=master)

Installing
----------

Clone the repository and install dependencies via [Composer](https://getcomposer.org):

```bash
$> git clone git@bitbucket.org:dangelidaniele/sainsbury-product-scraper.git
$> curl -sS https://getcomposer.org/installer | php
$> php composer.phar install
```

Running
-------

Once installed the vendors you should able to run the Tests

- Behat

```bash
$> bin/behat
```
- PhpUnit

```bash
$> bin/phpunit
```
Running
-------

Available command:

```bash
$> bin/console scrape-product-page
```

Use the option `--pretty-print` to visualize a pretty printed Json:

```bash
$> bin/console scrape-product-page --pretty-print
```

Optionally pass the _url_ as an argument:

```bash
$> bin/console scrape-product-page www.aproductpage.com --pretty-print
```

If an _url_ is not proved, a default one will be used

Directory Structure
--------------------

- __bin__: containing the executable bin
- __features__: behat scenarios and Context classes
- __src__: source files
- __tests__: phpunit tests
- __vendor__ the vendor folder

Directory src Structure
-----------------------

- __Application__: application layer, it contains the console command
- __Domain__: domain logic
- __Helper__: repository helper functions
- __Infrastructure__: infrastructure layer, it contains classes to consume a web page

Code Design
-----------
In the Domain Model folder has been created two classes:

- __Page__: which represent a scraped Page. It has a total and a list of Products
- __Product__: which represent a Product

These classes implement _JsonSerializable_. In fact, at the end of the process an instance of the class
Page is returned to the command: the command will call `json_encode($page)` to print in the console output the scraped
page information in the json format.

To fetch the products in a web page, in the Domain Fetcher folder has been created a class named __ProductPageWebFetcher__.

This class has a simple method:

```php

    public function fetchPageProducts($url)
    {
        $scrapedProductsArray = $this->productScraper->scrape($url);

        return Page::createFromProductKeyValueArray($scrapedProductsArray);
    }
```

The $productScraper is an object that implements the Domain Interface __WebProductScraper__.
This interface has defined as follow:

```php

    /**
     * @param $url
     * @return array $products
     */
    public function scrape($url);
```

It must return an array with the web page scraped information.

The concrete implementation of the productScraper is inside the folder Infrastructure/Scraper in a class named __WebScraper__.

It has ben placed in the Infrastructure folder because it is a law level layer detail, that interacts with the HTTP components
and, consequently, it is depended also from _Goutte_ (a dependency used in the project, see section _Dependencies_).


In the __WebScraper__ class has been implemented the logic to take the HTML from the provided url and "scrape" the page in order to
return the proper information into an array (finally used as showed above).

Dependencies
------------
To develop this sample application the following dependencies has been used:

- "fabpot/goutte": "~2.0",
- "symfony/console": "2.2"

_Goutte_ is a library for scraping a website.

The _SymfonyConsole_ has been used to generate the console command required

The following dependencies has been used in the dev-env:

- "behat/behat": "2.5.4",
- "phpunit/phpunit": "4.5.*"

See the composer.json for further details.


