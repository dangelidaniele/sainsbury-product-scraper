<?php

use Symfony\Component\Console\Tester\CommandTester;
use Erlangb\Scraper\Application\Console\Application;

require "MockPageContext.php";

class CommandContext extends MockPageContext
{
    protected $application;
    protected $command;

    /** @var  CommandTester */
    protected $tester;

    public function __construct(array $parameters)
    {
        $this->application = new Application($this->getGuzzleMockConf());
    }

    /**
     * @When /^I run command "([^"]*)"$/
     */
    public function iRunCommand($arg1)
    {
        $this->command = $this->application->get($arg1);

        if ($this->command === null) {
            throw new Exception(sprintf("Command %s Not Found", $this->command));
        }

        $this->tester = new CommandTester($this->command);

        $this->tester->execute(
            array(
                'command' => $this->command->getName(),
                'url' => "www.fakedomain.com"
            )
        );
    }

    /**
     * @Then /^The output should be in Json$/
     */
    public function theOutputShouldBeInJson()
    {
        $output = $this->getCommandOutput();
        PHPUnit_Framework_Assert::assertJson($output);
    }

    /**
     * @Then /^The output should contains "([^"]*)" product$/
     */
    public function theOutputShouldContainsProduct($numberOfProducts)
    {
        $output = $this->getCommandOutput();
        $outputArray = json_decode($output, true);

        PHPUnit_Framework_Assert::assertTrue(isset($outputArray["results"]));
        $totalProducts = $outputArray["results"];

        PHPUnit_Framework_Assert::assertEquals($numberOfProducts, count($totalProducts));
    }

    /**
     * @Then /^The output should have "([^"]*)" in the field total$/
     */
    public function theOutputShouldHaveInTheFieldTotal($total)
    {
        $output = $this->getCommandOutput();
        $outputArray = json_decode($output, true);

        PHPUnit_Framework_Assert::assertTrue(isset($outputArray["total"]));
        $totalUnitPrices = $outputArray["total"];

        PHPUnit_Framework_Assert::assertEquals($total, $totalUnitPrices);
    }

    /**
     * @Then /^The output should have empty result$/
     */
    public function theOutputShouldHaveEmptyResult()
    {
        $output = $this->getCommandOutput();
        $outputArray = json_decode($output, true);

        $totalUnitPrices = $outputArray["total"];
        $results = $outputArray["results"];

        PHPUnit_Framework_Assert::assertEquals(0, $totalUnitPrices);
        PHPUnit_Framework_Assert::assertEquals(0, count($results));
    }

    private function getCommandOutput()
    {
        return $this->tester->getDisplay();
    }
}
