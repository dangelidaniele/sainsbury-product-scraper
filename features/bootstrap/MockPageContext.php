<?php

use Behat\Behat\Context\BehatContext;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Stream\Stream;
use GuzzleHttp\Subscriber\Mock;
use Behat\Gherkin\Node\TableNode;

class MockPageContext extends BehatContext
{
    /** @var  Mock */
    protected $mock;

    /**
     * @Given /^The product page "([^"]*)" exists$/
     */
    public function theProductPageExists($name)
    {
        $content = $this->getMockHtmlPageContents($name);
        $this->addMockRepsonseToGuzzleClient($content);
    }

    /**
     * @Given /^The page has the following product links$/
     */
    public function thePageHasTheFollowingProductLinks(TableNode $table)
    {
        foreach ($table->getRows() as $row) {
            $content = $this->getMockHtmlPageContents($row[0]);
            $this->addMockRepsonseToGuzzleClient($content);
        }
    }

    public function getGuzzleMockConf()
    {
        $this->mock = new Mock();

        $guzzle = new Client(array('redirect.disable' => true, 'base_url' => ''));

        $guzzle->getEmitter()->attach($this->mock);

        return ['guzzle_client' => $guzzle];
    }

    /**
     * @Given /^I request a page with the following html "([^"]*)"$/
     */
    public function iRequestAPageWithTheFollowingHtml($arg1)
    {
        $this->mock->addResponse(new Response(200, array(), Stream::factory($arg1)));
    }

    private function getMockHtmlPageContents($mockPageName)
    {
        $path = __DIR__ . "/../Resources/pages/" . $mockPageName;

        if (!file_exists($path)) {
            throw new Exception(sprintf("The file %s doesn't exists", $path));
        }

        return file_get_contents($path);
    }

    private function addMockRepsonseToGuzzleClient($mockResponse)
    {
        $this->mock->addResponse(new Response(200, array(), Stream::factory($mockResponse)));
    }
}
