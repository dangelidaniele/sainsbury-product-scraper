Feature: Retrieve product information on a Sainsbury's Grocery site page.
  For each product will be displayed the name, the relative page size in KB and the unit_price.
  Display also a field named total as the sum of all the product's unit_price in the page.
  The format to display should be JSON

  Scenario: The scrape-product-page command must display information in JSON format
    Given The product page "page1.html" exists
    And The page has the following product links
      | product_avocado.html |
      | product_with_no_description.html |
    When I run command "scrape-product-page"
    Then The output should be in Json

  Scenario: The scrape-product-page command must return a list of products
    Given The product page "page1.html" exists
    And The page has the following product links
      | product_avocado.html |
      | product_with_no_description.html |
    When I run command "scrape-product-page"
    Then The output should contains "2" product

  Scenario: The scrape-product-page command must return the sum of the product's prices
    Given The product page "page1.html" exists
    And The page has the following product links
      | product_avocado.html |
      | product_with_no_description.html |
    When I run command "scrape-product-page"
    Then The output should have "3.30" in the field total

  Scenario: The scrape-product-page command must return an empty result if the page doesn't have products
    Given The product page "pageWithoutProducts.html" exists
    When I run command "scrape-product-page"
    Then The output should have empty result

  Scenario: The scrape-product-page command must return an empty result if the page does not have expected HTML
    Given The product page "pageWithProductButDIfferentExpectedHtml.html" exists
    When I run command "scrape-product-page"
    Then The output should have empty result
