<?php

namespace Erlangb\Scraper\Application\Console;

use Erlangb\Scraper\Application\Console\Command\ProductCommand;
use Erlangb\Scraper\Domain\Fetcher\ProductPageWebFetcher;
use Erlangb\Scraper\Infrastructure\Scraper\WebScraper;

use Symfony\Component\Console\Application as BaseApplication;

class Application extends BaseApplication
{
    public function __construct(array $conf = array())
    {
        parent::__construct('Product Scraper', "0.1");

        $webScraper = new WebScraper($conf);

        $productWebProductPageFetcher = new ProductPageWebFetcher($webScraper);
        $this->add(new ProductCommand($productWebProductPageFetcher));
    }

    public function getLongVersion()
    {
        $version = parent::getLongVersion().' by <comment>daniel D\'Angeli</comment>';

        return $version;
    }

}
