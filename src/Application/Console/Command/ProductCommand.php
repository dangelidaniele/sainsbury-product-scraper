<?php

namespace Erlangb\Scraper\Application\Console\Command;

use Erlangb\Scraper\Domain\Fetcher\ProductPageWebFetcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCommand extends Command
{
    const COMMAND_NAME = "scrape-product-page";
    const BASE_URL = "http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?listView=true&orderBy=FAVOURITES_FIRST&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0&pageSize=20&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151&promotionId=#langId=44&storeId=10151&catalogId=10137&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0&hideFilters=true";

    private $productPageWebFetcher;

    public function __construct(ProductPageWebFetcher $productPageFetcher)
    {
        parent::__construct();
        $this->productPageWebFetcher = $productPageFetcher;
    }

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->addArgument(
                'url',
                InputArgument::OPTIONAL,
                'Url to scrape'
            )
            ->addOption("pretty-print")
            ->setDescription('Scrape Sainsbury\'s product page');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument("url");

        if (null === $url) {
            $url = self::BASE_URL;
        }

        $page = $this->productPageWebFetcher->fetchPageProducts($url);

        if (false === $input->getOption("pretty-print")) {
            $output->writeln(json_encode($page));
        } else {
            $output->writeln(json_encode($page, JSON_PRETTY_PRINT));
        }
    }
}
