<?php

namespace Erlangb\Scraper\Domain\Exception;

use Erlangb\Scraper\Exception;

class EmptyScrapedPageException extends \Exception implements Exception
{
}
