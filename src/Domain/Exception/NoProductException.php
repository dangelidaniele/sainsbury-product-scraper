<?php

namespace Erlangb\Scraper\Domain\Exception;

use Erlangb\Scraper\Exception;

class NoProductException extends \Exception implements Exception
{

}
