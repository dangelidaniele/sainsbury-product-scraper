<?php

namespace Erlangb\Scraper\Domain\Exception;

use Erlangb\Scraper\Exception;

class NotNumberException extends \Exception implements Exception
{
}
