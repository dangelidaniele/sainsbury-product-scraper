<?php

namespace Erlangb\Scraper\Domain\Fetcher;

use Erlangb\Scraper\Domain\Model\Page;
use Erlangb\Scraper\Domain\Scraper\WebProductScraper;

class ProductPageWebFetcher
{
    private $productScraper;

    public function __construct(WebProductScraper $productScraper)
    {
        $this->productScraper = $productScraper;
    }

    /**
     * @param $url
     * @return Page
     */
    public function fetchPageProducts($url)
    {
        $scrapedProductsArray = $this->productScraper->scrape($url);

        return Page::createFromProductKeyValueArray($scrapedProductsArray);
    }
}
