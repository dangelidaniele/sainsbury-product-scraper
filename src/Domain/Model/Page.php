<?php

namespace Erlangb\Scraper\Domain\Model;
use Erlangb\Scraper\Helper\NumberHelper;

/**
 * Class Page
 * @package Erlangb\Scraper\Domain\Model
 */
class Page implements \JsonSerializable
{
    /** @var  Product[] $product */
    private $products;

    /** @var  float */
    private $total;

    public function __construct(array $products)
    {
        $this->products = $products;
        $this->total = $this->calculateTotal();
    }

    public static function createFromProductKeyValueArray(array $productsArray)
    {
        $products = [];

        foreach ($productsArray as $productArray) {
            $products[] = Product::createFromKeyValueArray($productArray);
        }

        return new Page($products);
    }

    public function calculateTotal()
    {
        $total = 0;

        foreach ($this->products as $product) {
            $total += $product->getUnitPrice();
        }

        return NumberHelper::floadDoublePrecision($total);
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return ["results" => $this->products, "total" => $this->total];
    }
}
