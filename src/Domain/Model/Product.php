<?php

namespace Erlangb\Scraper\Domain\Model;

class Product implements \JsonSerializable
{
    private $title;
    private $description;
    private $size;
    private $unitPrice;

    public function __construct($title, $description, $size, $unitPrice)
    {
        $this->title = $title;
        $this->description = $description;
        $this->size = $size;
        $this->unitPrice = $unitPrice;
    }

    public static function createFromKeyValueArray(array $product)
    {
        return new Product(
            $product["title"],
            $product["description"],
            $product["size"],
            $product["unitPrice"]
        );
    }

    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return [
            "title" => $this->title,
            "description" => $this->description,
            "size" => $this->size,
            "unit_price" => $this->unitPrice
        ];
    }
}
