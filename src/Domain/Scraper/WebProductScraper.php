<?php

namespace Erlangb\Scraper\Domain\Scraper;

interface WebProductScraper
{
    /**
     * @param $url
     * @return array $products
     */
    public function scrape($url);
}
