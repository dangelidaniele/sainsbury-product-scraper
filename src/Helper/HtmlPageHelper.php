<?php

namespace Erlangb\Scraper\Helper;

class HtmlPageHelper
{
    public static function getPageSizeInKb($html)
    {
        $size = mb_strlen($html) / 1024;
        $floatSize = NumberHelper::strNumberToFloatWithDoublePrecision($size);

        return $floatSize;
    }
}

