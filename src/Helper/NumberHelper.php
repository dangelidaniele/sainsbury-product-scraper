<?php

namespace Erlangb\Scraper\Helper;

use Erlangb\Scraper\Domain\Exception\NotNumberException;

class NumberHelper
{
    const DOUBLE_PRECISION = 2;

    public static function getPriceInString($str)
    {
        $s = preg_replace("/[^0-9\.]/", "", $str);

        return $s;
    }

    public static function strNumberToFloatWithDoublePrecision($strNumber)
    {
        if (is_numeric($strNumber)) {
            $floatValue = (float) $strNumber;

            return self::roundFloatToDoublePrecision($floatValue);
        }

        throw new NotNumberException(sprintf("The string %s not contain a valid number", $strNumber));
    }

    public static function floadDoublePrecision($number)
    {
        if (is_numeric($number)) {
            return self::roundFloatToDoublePrecision($number);
        }

        throw new NotNumberException(sprintf("The string %s not contain a valid number", $strNumber));
    }

    private static function roundFloatToDoublePrecision($floatNumber)
    {
        return round($floatNumber, self::DOUBLE_PRECISION);
    }
}
