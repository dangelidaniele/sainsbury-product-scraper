<?php

namespace Erlangb\Scraper\Infrastructure\Scraper;

use Erlangb\Scraper\Helper\NumberHelper;
use Erlangb\Scraper\Domain\Model\Page;
use Erlangb\Scraper\Domain\Model\Product;
use Goutte\Client;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DomCrawler\Crawler;
use Erlangb\Scraper\Domain\Scraper\WebProductScraper;
use Erlangb\Scraper\Domain\Exception\NoProductException;
use Erlangb\Scraper\Helper\HtmlPageHelper;

class WebScraper implements WebProductScraper
{
    protected $client;

    public function __construct(array $conf = array())
    {
        $this->client = new Client();
        $this->configure($conf);
    }

    private function configure(array $conf)
    {
        if (isset($conf["guzzle_client"])) {
            $this->client->setClient($conf["guzzle_client"]);
        }
    }

    /**
     * @param $url
     * @return array $products
     */
    public function scrape($url)
    {
        $products = $this->scrapeProducts($url);

        return $products;
    }

    protected function scrapeProducts($url)
    {
        $crawler = $this->crawlUrl($url);
        $productCrawler = $this->filterProductCrawler($crawler);
        $products = $this->filterProducts($productCrawler);

        return $products;
    }

    protected function filterProductCrawler(Crawler $crawler)
    {
        return $crawler->filterXPath('//div[@class="product "]');
    }

    protected function filterProducts(Crawler $crawler)
    {
        $elements = [];

        foreach ($crawler as $node) {
            $productCrawler = new Crawler($node);

            $productLink = $this->filterProductLink($productCrawler);
            $productTitle = $this->filterProductTitle($productCrawler);
            $productUnitPrice = $this->filterProductUnitPrice($productCrawler);

            $crawlerLink = $this->crawlUrl($productLink);

            $productSize = sprintf("%sKB", HtmlPageHelper::getPageSizeInKb($crawlerLink->html()));
            $productDescription = $this->filterProductDescription($crawlerLink);

            $elements[] = [
                'title' => $productTitle,
                'description' => $productDescription,
                'size' => $productSize,
                'unitPrice' => $productUnitPrice
            ];
        }

        return $elements;
    }

    /**
     * @param $url
     * @return Crawler
     */
    protected function crawlUrl($url)
    {
        $crawler = $this->client->request("GET", $url);

        if (null !== $crawler) {
            return $crawler;
        }

        throw new EmptyScrapedPageException(sprintf("The page at url %s is returning an empty Crawler", $url));
    }

    protected function filterProductLink(Crawler $productCrawler)
    {
        $link = $productCrawler->filter('a')->extract(array('href'));
        $this->checkExtractedLinkInformation($link);

        return trim($link[0]);
    }

    protected function filterProductTitle(Crawler $productCrawler)
    {
       $link = $productCrawler->filter('a')->extract(array('_text'));
       $this->checkExtractedLinkInformation($link);

        return trim($link[0]);
    }

    private function checkExtractedLinkInformation($link)
    {
        if (null === $link || count($link) == 0) {
            throw new NoProductException("No Title in Product");
        }
    }

    protected function filterProductUnitPrice(Crawler $productCrawler)
    {
        $floatUnitPrice = null;

        $pUnitPrice = $productCrawler
            ->filterXPath('//div[@class="pricing"]')
            ->filter("p.pricePerUnit");

        if ($pUnitPrice->count() > 0) {
            $number = NumberHelper::getPriceInString($pUnitPrice->text());
            $floatUnitPrice = NumberHelper::strNumberToFloatWithDoublePrecision($number);

        }

        return $floatUnitPrice;
    }

    protected function filterProductDescription(Crawler $productPageCrawler)
    {
        $description = "";
        $productCrawlerContainer = $productPageCrawler->filterXPath('//div[@class="itemTypeGroupContainer productText"]');

        foreach ($productCrawlerContainer as $node) {
            $productCrawlerText = new Crawler($node);
            $h3Text = $productCrawlerText->filter("h3")->text();
            if ($h3Text == "Description") {
                $productDesc = $productCrawlerText->filter("p");
                foreach ($productDesc as $productDescNode) {
                    $descriptionElementCrawler = new Crawler($productDescNode);
                    $description .= $descriptionElementCrawler->text();
                }
            }
        }

        return $description;
    }
}
