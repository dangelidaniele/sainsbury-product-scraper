<?php

namespace Erlangb\Scraper\Application\Console;

use Erlangb\Scraper\Application\Console\Command\ProductCommand;

class ApplicationTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Application  */
    protected $application;

    public function setUp()
    {
        $this->application = new Application();
    }

    public function testItHasProductCommand()
    {
        $this->assertTrue($this->application->has(ProductCommand::COMMAND_NAME));
    }

    public function testItAddAValidProductCommand()
    {
        $command = $this->application->get(ProductCommand::COMMAND_NAME);

        $this->assertInstanceOf(
            'Erlangb\Scraper\Application\Console\Command\ProductCommand',
            $command
        );
    }
}
