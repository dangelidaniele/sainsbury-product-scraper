<?php

namespace Erlangb\Scraper\Application\Console\Command;

class ProductCommandTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ProductCommand  */
    protected $productCommand;

    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $productWebPageFetcherMock;

    public function setUp()
    {
        $this->productWebPageFetcherMock = $this->anyProductWebPageFetcherMock();
        $this->productCommand = new ProductCommand($this->productWebPageFetcherMock);
    }

    public function testItHasAValidName()
    {
        $this->assertEquals(ProductCommand::COMMAND_NAME, $this->productCommand->getName());
    }

    public function testExecute()
    {
        $urlToScrape = "anyUrl";
        $expectedResponse = ["results" => [1,2], "total" => 10];

        $input = $this->getInputMockWithUrlArgument($urlToScrape);
        $output = $this->getOutputMock();

        $this->productWebPageFetcherMock->expects($this->once())
            ->method("fetchPageProducts")
            ->with($this->equalTo($urlToScrape))
            ->willReturn($this->anyPageMock($expectedResponse));

        $output->expects($this->once())
            ->method("writeln")
            ->with(json_encode(["results" => [1,2], "total" => 10]));

       $this->productCommand->run($input, $output);

    }

    /**
     * @param $url
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getInputMockWithUrlArgument($url)
    {
        $input = $this->getMockBuilder('Symfony\Component\Console\Input\ArgvInput')
            ->disableOriginalConstructor()
            ->getMock();

        $input
            ->expects($this->once())
            ->method("getArgument")
            ->with($this->equalTo("url"))
            ->willReturn($url);

        $input
            ->expects($this->once())
            ->method("getOption")
            ->with($this->equalTo("pretty-print"))
            ->willReturn(false);

        return $input;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getOutputMock()
    {
        return $this->getMockBuilder('Symfony\Component\Console\Output\StreamOutput')
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function anyProductWebPageFetcherMock()
    {
        return $this->getMockBuilder('Erlangb\Scraper\Domain\Fetcher\ProductPageWebFetcher')
            ->getMock();
    }

    private function anyPageMock($expectedResponse)
    {
        $page = $this->getMockBuilder('Erlangb\Scraper\Domain\Model\Page')
            ->disableOriginalConstructor()
            ->getMock();

        $page->expects($this->once())
            ->method("jsonSerialize")
            ->willReturn($expectedResponse);

        return $page;
    }
}
