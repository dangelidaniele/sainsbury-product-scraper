<?php

namespace Erlangb\Scraper\Domain\Fetcher;

class ProductPageWebFetcherTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ProductPageWebFetcher */
    protected $productPageWebFetcher;

    /** @var  \PHPUnit_Framework_MockObject_MockObject */
    protected $productScraperMock;

    public function setUp()
    {
        $this->productScraperMock = $this->anyProductScraperMock();
        $this->productPageWebFetcher = new ProductPageWebFetcher($this->productScraperMock);
    }

    public function testItShouldReturnAPageFromScrapedProductArray()
    {
        $urlToFetch = "www.onesite.com/productpage";

        $this->productScraperReturnResponseFromUrl(
            [
                ['title' => 'a title', 'description' => 'desc', 'unitPrice' => 1.30, 'size' => "10KB"],
                ['title' => 'a title', 'description' => 'desc', 'unitPrice' => 1.30, 'size' => "10KB"],
            ],
            $urlToFetch
        );

        $page = $this->productPageWebFetcher->fetchPageProducts($urlToFetch);
        $this->assertIsAPage($page);
    }

    private function assertIsAPage($page)
    {
        $this->assertEquals('Erlangb\Scraper\Domain\Model\Page', get_class($page));
    }

    private function productScraperReturnResponseFromUrl(array $response, $url)
    {
        $this->productScraperMock->expects($this->once())
            ->method("scrape")
            ->with($url)
            ->willReturn($response);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function anyProductScraperMock()
    {
        return $this->getMockBuilder('Erlangb\Scraper\Domain\Scraper\WebProductScraper')
            ->disableOriginalConstructor()
            ->getMock();
    }

}
