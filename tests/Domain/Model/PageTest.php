<?php

namespace Erlangb\Scraper\Domain\Model;

class PageTest extends \PHPUnit_Framework_TestCase
{
    /** @var Page */
    protected $page;

    public function setUp()
    {
        $this->page = new Page($this->getTwoProductsMockArray());
    }

    public function testItCalculateTotal()
    {
        $this->assertEquals(6.20, $this->page->calculateTotal());
    }

    public function testItShouldBeJsonSerializable()
    {
        $json = json_encode($this->page);

        $dataArray = json_decode($json, true);

        $this->assertEquals(2, count($dataArray["results"]));
        $this->assertEquals(6.20, $dataArray["total"]);
    }

    private function getTwoProductsMockArray()
    {
        $productMockOne = $this->getMockBuilder('Erlangb\Scraper\Domain\Model\Product')
            ->setConstructorArgs(["title", "desc", "10kb", 2.60])
            ->getMock();

        $productMockOne->expects($this->any())
            ->method("getUnitPrice")
            ->willReturn(2.60);

        $productMockTwo = $this->getMockBuilder('Erlangb\Scraper\Domain\Model\Product')
            ->setConstructorArgs(["titleTwo", "descTwo", "20kb", 3.60])
            ->getMock();

        $productMockTwo->expects($this->any())
        ->method("getUnitPrice")
        ->willReturn(3.60);

        return [$productMockOne, $productMockTwo];
    }
}
