<?php

namespace Erlangb\Scraper\Domain\Model;

class ProductTest extends \PHPUnit_Framework_TestCase
{
    protected $product;

    public function setUp()
    {
        $this->product = new Product("title", "desc", "10kb", 2.60);
    }

    public function testItShouldBeJsonSerializable()
    {
        $json = json_encode($this->product);

        $dataArray = json_decode($json, true);

        $this->assertEquals("title", $dataArray["title"]);
        $this->assertEquals("desc", $dataArray["description"]);
        $this->assertEquals("10kb", $dataArray["size"]);
        $this->assertEquals(2.60, $dataArray["unit_price"]);
    }
}
