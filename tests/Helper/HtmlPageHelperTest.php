<?php

namespace Erlangb\Scraper\Helper;

class HtmlPageHelperTest extends \PHPUnit_Framework_TestCase
{
    public function testItGetPageSize()
    {
        //see ls -l it display 23931 bytes i.e 23930/102 = 23.369 = 23.37
        $html = file_get_contents(__DIR__."/../Resources/llorem-ipsum.html");
        $this->assertEquals(23.37, HtmlPageHelper::getPageSizeInKb($html));
    }
}
