<?php

namespace Erlangb\Scraper\Helper;

class NumberHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getPriceString
     */
    public function testItGetPriceInString($str, $expected)
    {
        $priceStr = NumberHelper::getPriceInString($str);
        $this->assertEquals($expected, $priceStr);
    }

    /**
     * @dataProvider getPriceFloatString
     */
    public function testItConvertAStringNumberInAFloatWithDoublePrecision($str, $expected)
    {
        $floatPrice = NumberHelper::strNumberToFloatWithDoublePrecision($str);
        $this->assertEquals($expected, $floatPrice);
    }

    /**
     * @expectedException \Erlangb\Scraper\Domain\Exception\NotNumberException
     */
    public function testItRaiseExceptionIfStringIsNotAValidNumber()
    {
        $floatPrice = NumberHelper::strNumberToFloatWithDoublePrecision("a not number");
    }

    public function getPriceString()
    {
        return [
            ["<span>1.<span>34</span>", "1.34"],
            ["1.34", "1.34"],
            ["this is my price 103.32", "103.32"],
            ["this is an empty price", ""],
        ];
    }

    public function getPriceFloatString()
    {
        return [
            ["1.000000", 1.00],
            ["1.34", 1.34],
            ["1", 1.00],
            ["0.543331", 0.54]
        ];
    }
}
