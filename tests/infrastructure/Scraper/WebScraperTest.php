<?php

namespace Erlangb\Scraper\infrastructure\Scraper;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class WebScraperExposed to Test protected methods
 * @package Erlangb\Scraper\infrastructure\Scraper
 */
class WebScraperExposed extends WebScraper
{
    public function filterProductDescription(Crawler $productPageCrawler)
    {
        return parent::filterProductDescription($productPageCrawler);
    }

    public function filterProductUnitPrice(Crawler $productCrawler)
    {
        return parent::filterProductUnitPrice($productCrawler);
    }

    public function filterProductLink(Crawler $productPageCrawler)
    {
        return parent::filterProductLink($productPageCrawler);
    }

    public function filterProductTitle($productCrawler)
    {
        return parent::filterProductTitle($productCrawler);
    }
}

class WebScraperTest extends \PHPUnit_Framework_TestCase
{
    /** @var  WebScraperExposed */
    protected $webScraper;

    public function setUp()
    {
        $this->webScraper = new WebScraperExposed(array());
    }

    public function testItFilterProductDescription()
    {
        $crawler = $this->getMockBuilder('Symfony\Component\DomCrawler\Crawler')
            ->disableOriginalConstructor()
            ->getMock();

        $crawler->expects($this->once())
            ->method("filterXPath")
            ->willReturn(["<div><h3>Description</h3><p>One</p><p>description</p></div>"]);

        $productDesc = $this->webScraper->filterProductDescription($crawler);

        $this->assertEquals("Onedescription", $productDesc);
    }

    public function testItGetEmptyStringIfProductDescriptionDoesNottExists()
    {
        $crawler = $this->anyProductCrawlerMock();

        $crawler->expects($this->once())
            ->method("filterXPath")
            ->willReturn(["<div><h3>Another</h3><p><p>description</p></div>"]);

        $productDesc = $this->webScraper->filterProductDescription($crawler);

        $this->assertEquals("", $productDesc);
    }

    public function testItFilterUnitPrice()
    {
        $crawler = $this->anyProductCrawlerMock();

        $crawler->expects($this->once())
            ->method("filterXPath")
            ->with($this->equalTo('//div[@class="pricing"]'))
            ->willReturn($crawler);

        $crawler->expects($this->once())
            ->method("filter")
            ->with("p.pricePerUnit")
            ->willReturn($crawler);

        $crawler->expects($this->once())
            ->method("text")
            ->willReturn("$1.80/unit");

        $crawler->expects($this->once())
            ->method("count")
            ->willReturn(1);

        $price = $this->webScraper->filterProductUnitPrice($crawler);
        $this->assertEquals(1.80, $price);
    }

    public function testItReturnNullIfUnitPriceDoesntExists()
    {
        $crawler = $this->anyProductCrawlerMock();

        $crawler->expects($this->once())
            ->method("filterXPath")
            ->with($this->equalTo('//div[@class="pricing"]'))
            ->willReturn($crawler);

        $crawler->expects($this->once())
            ->method("filter")
            ->with("p.pricePerUnit")
            ->willReturn($crawler);

        $crawler->expects($this->never())
            ->method("text");

        $crawler->expects($this->once())
            ->method("count")
            ->willReturn(0);

        $this->webScraper->filterProductUnitPrice($crawler);
    }

    public function testItFilterLinkFromProductCrawler()
    {
        $expectedLink = "www.link.com";

        $crawler = $this->anyProductCrawlerMock();

        $this->crawlerFilterLink($crawler);
        $this->crawlerExtract($crawler, ['href'], [$expectedLink]);

        $link = $this->webScraper->filterProductLink($crawler);
        $this->assertEquals($expectedLink, $link);
    }

    /**
     * @expectedException \Erlangb\Scraper\Domain\Exception\NoProductException
     */
    public function testItShouldRaiseExceptionIfProductDoesntHaveLink()
    {
        $expectedLink = "www.link.com";

        $crawler = $this->anyProductCrawlerMock();

        $this->crawlerFilterLink($crawler);
        $this->crawlerExtract($crawler, ['href'], null);

        $this->webScraper->filterProductLink($crawler);
    }

    /**
     * @expectedException \Erlangb\Scraper\Domain\Exception\NoProductException
     */
    public function testItShouldRaiseExceptionIfProductHasAnEmptyLink()
    {
        $expectedLink = "www.link.com";

        $crawler = $this->anyProductCrawlerMock();

        $this->crawlerFilterLink($crawler);
        $this->crawlerExtract($crawler, ['href'], []);

        $this->webScraper->filterProductLink($crawler);
    }

    public function testItFilterProductTitle()
    {
        $expectedTitle = "titleA";

        $crawler = $this->anyProductCrawlerMock();

        $this->crawlerFilterLink($crawler);
        $this->crawlerExtract($crawler, ['_text'], [$expectedTitle]);

        $title = $this->webScraper->filterProductTitle($crawler);
        $this->assertEquals($expectedTitle, $title);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function anyProductCrawlerMock()
    {
        return $this->getMockBuilder('Symfony\Component\DomCrawler\Crawler')
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function crawlerFilterLink($crawlerMock)
    {
        $crawlerMock->expects($this->once())
            ->method("filter")
            ->with($this->equalTo('a'))
            ->willReturn($crawlerMock);
    }

    private function crawlerExtract($crawlerMock, $expectInput, $expectedOutput)
    {
        $crawlerMock->expects($this->once())
            ->method("extract")
            ->with($this->equalTo($expectInput))
            ->willReturn($expectedOutput);
    }
}
